package hr.fer.ruazosa.stopwatch

import android.os.AsyncTask
import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import android.widget.Toast
import kotlinx.android.synthetic.main.activity_main.*
import java.lang.StringBuilder

class MainActivity : AppCompatActivity() {

    var doInBackGround: AsyncTask<Long, Long, Unit>? = null
    @Volatile private var counter_msec_last_val: Long = 0

    // funkcija koja sluzi za pretvrobu brojcane vrijednosti
    // u String prikaz unutar TextViewa
    fun setStopWatchTimePassedTextViewString(counter_msec: Long){
        val counter_sec: Long = counter_msec / 1000
        val minutes: Long = counter_sec / 60
        val seconds: Long = counter_sec % 60
        val text_builder = StringBuilder()

        if(minutes < 10){
            text_builder.append("0")
        }
        text_builder.append(minutes.toString())
        text_builder.append(":")
        if(seconds < 10){
            text_builder.append("0")
        }
        text_builder.append(seconds.toString())

        stopwatchTimePassedTextView.text = text_builder.toString()
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        // na pocetku omoguci akciju klikanja samo na play gumbu
        playFloatingActionButton.isEnabled = true
        pauseFloatingActionButton.isEnabled = false
        stopFloatingActionButton.isEnabled = false
        stopwatchStateTextView.text = getString(R.string.stopwatch_halted_string)


        playFloatingActionButton.setOnClickListener {
            // Toast je za provjeru ispravnosti isEnabled postavljanja, koristeno za debug
            //Toast.makeText(applicationContext, "Play button click listener called!", Toast.LENGTH_LONG).show()
            //Log.d("StopWatch_Debug", "MainThread --> counter_msec_last_val: " + counter_msec_last_val.toString())
            playFloatingActionButton.isEnabled = false
            pauseFloatingActionButton.isEnabled = true
            stopFloatingActionButton.isEnabled = true
            doInBackGround = DoInBackGround()
            doInBackGround?.execute(counter_msec_last_val)
            stopwatchStateTextView.text = getString(R.string.stopwatch_running_string)
        }

        pauseFloatingActionButton.setOnClickListener {
            // pauziraj brojac
            // Toast je za provjeru ispravnosti isEnabled postavljanja, koristeno za debug
            //Toast.makeText(applicationContext, "Pause button click listener called!", Toast.LENGTH_LONG).show()
            playFloatingActionButton.isEnabled = true
            pauseFloatingActionButton.isEnabled = false
            stopFloatingActionButton.isEnabled = true
            if(!doInBackGround!!.isCancelled) {
                doInBackGround?.cancel(true)
            }
            stopwatchStateTextView.text = getString(R.string.stopwatch_paused_string)
            //setStopWatchTimePassedTextViewString(counter_msec_last_val)
        }

        stopFloatingActionButton.setOnClickListener {
            // zaustavi brojac
            // Toast je za provjeru ispravnosti isEnabled postavljanja
            //Toast.makeText(applicationContext, "Stop button click listener called!", Toast.LENGTH_LONG).show()
            playFloatingActionButton.isEnabled = true
            pauseFloatingActionButton.isEnabled = false
            stopFloatingActionButton.isEnabled = false
            if(!doInBackGround!!.isCancelled) {
                doInBackGround?.cancel(true)
            }
            //reset counter value
            counter_msec_last_val = 0
            stopwatchStateTextView.text = getString(R.string.stopwatch_halted_string)
            setStopWatchTimePassedTextViewString(counter_msec_last_val)
        }
    }

    override fun onDestroy() {
        doInBackGround?.cancel(true)
        super.onDestroy()
    }

    inner class DoInBackGround: AsyncTask<Long, Long, Unit>() {

        var counter_msec: Long = 0

        override fun onPostExecute(result: Unit?) {
            super.onPostExecute(result)
        }
        override fun doInBackground(vararg params: Long?) {
            counter_msec = params[0]!!
            while(true) {
                if (this.isCancelled) {
                    break
                }
                counter_msec += 100
                if((counter_msec.toInt() % 1000) == 0 ) {
                    this.publishProgress(counter_msec)
                }
                Thread.sleep(100)
            }
        }

        override fun onProgressUpdate(vararg values: Long?) {
            setStopWatchTimePassedTextViewString(values[0]!!)
            // postavi brojac unutar glavne aktivnosti na trenutnu vrijednost unutar AsynTaska
            // ThreadSafe???
            //Log.d("StopWatch_Debug", "CounterThread --> counter_msec_last_val: " + counter_msec)
            counter_msec_last_val = counter_msec
        }
        override fun onPreExecute() {
            super.onPreExecute()
        }
    }
}
